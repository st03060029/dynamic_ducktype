import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Dynamic_DuckType {
    /**
     * 如果它走路像隻鴨子，叫聲像個鴨子，那它就是鴨子。
     * If it walks like a duck and it quacks like a duck, then it must be a duck
     * */
    public static void main(String[] args)
    {
        try {
            new Dynamic_DuckType().doQuack(new Something());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void doQuack(Object obj) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method quack = obj.getClass().getMethod("quack", null);
        quack.invoke(obj, null);
    }

}
